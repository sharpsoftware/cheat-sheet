---
Vue
---

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Classes     <vue/classes.rst>
   Components  <vue/components.rst>
   Reactivity  <vue/reactivity.rst>
   Forms       <vue/forms.rst>
   Slots       <vue/slots.rst>

Vue code may be :ref:`embedded` directly into a web page (for smaller projects) or developed :ref:`standalone` and included (For larger ones).

Use `SFC <https://sfc.vuejs.org/>`_ to test out throw away code (See `SFC Code Review <https://youtu.be/CcDWPyA6dwU>`_).

Development
===========

Embedded
--------

.. authordate:

Vue is embedded most readily through a global import as firefox does not currently support importing modules.

.. tabs::

    .. code-tab:: typescript ECMA Script Modules

        <script type="importmap">
          {
            "imports": {
              "vue": "https://unpkg.com/vue@3/dist/vue.esm-browser.js"
            }
          }
        </script>

        <div id="app">{{ message }}</div>

        <script type="module">
          import { createApp } from 'vue'

          createApp({
            data() {
              return {
                message: 'Hello Vue!'
              }
            }
          }).mount('#app')
        </script>

    .. code-tab:: typescript Global Import

        <script src="https://unpkg.com/vue@3"></script>

        <div id="app">{{ message }}</div>

        <script>
          const { createApp } = Vue

          createApp({
            data() {
              return {
                message: 'Hello Vue!'
              }
            }
          }).mount('#app')
        </script>

.. note::

    The above may be used with minor modification for `production <https://vuejs.org/guide/best-practices/production-deployment.html#without-build-tools>`_.

Compiled
--------

Vue projects are initiated through either `create-vue <https://vitejs.dev/guide/>`_ (Vite based) or `vue-cli <https://cli.vuejs.org/guide/creating-a-project.html>`_ (Webpack) based.

.. tabs::

    .. code-tab:: shell Vite

        npm create vite@latest
        # OR
        yarn create vite
        # OR
        pnpm create vite

    .. code-tab:: shell Vue-CLI

        vue create hello-world
        # OR
        vue ui


.. todo::

    Ask Michael how he is initializing projects vue-cli/vite ?
    Ask what he installs after initializing a project.

### Environment Files

The order in which env files are stacked are as follows ::

  .env < .env.local < .env.MODE < .env.MODE.local

`Digital Ocean: Environment Variables <https://www.digitalocean.com/community/tutorials/vuejs-working-with-environment-variables>`_ covers the usage of environment files.
To reference a variable in the prior files use the following ::

  VALUE = ${VALUE} # Doesn't work it seems

For :file:`.env` to javascript/typescript use `dotenv <https://stackoverflow.com/q/48781629>`_ and perhaps `dotenv-expand <https://github.com/motdotla/dotenv-expand>`_.


.. note::

  Be very careful when placing using ref and v-if together