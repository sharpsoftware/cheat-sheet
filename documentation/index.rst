.. Sharp Software Solution: Cheat Sheet documentation master file, created by
   sphinx-quickstart on Thu Jun  9 22:56:33 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Software: Cheat Sheet
=====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

..   Documentation  <documentation.rst>
..   CSharp         <c_sharp.rst>
..   Javascript     <javascript.rst>
..   PHP            <php.rst>
..   Glossary       <glossary.rst>

A collection of notes centered around software development authored during my time at various development houses.

Authors are to review :ref:`Documentation` before contributing to this document.

TO DO
=====

.. todonotes

.. todo::

   Pythons' lexer library supports the typesetting go GCode which might be of interest.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
