Glossary
--------

.. glossary::

    De-Git
        Helper that will pull down the latest distribution of a repository, that is without the version control folder.
        This is used primarily for pulling down templates where one will subsequently initialize the repository.

    Reactivity
        Reactivity is a programming paradigm that allows us to adjust to changes in a declarative manner.